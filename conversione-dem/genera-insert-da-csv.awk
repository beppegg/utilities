#!/usr/bin/awk -f
BEGIN{
    FS = ";|\n";
    
    columnNames["SERVIZI_PREMIUM"] = "PREMIUM_CODE";
    columnNames["CODICE_LAVORAZIONE"] = "OPERATION_TYPE_CODE";
    columnNames["COD_CONTRATTO"] = "CONTRACT_CODE";
    columnNames["CODICE_PROMOZIONE"] = "PROMO_BIS_CODE";
    columnNames["TREATMENTCODE"] = "PROMO_CODE";
    columnNames["TREATMENT_CODE"] = "PROMO_CODE";
    columnNames["CODICE_SERVIZIO_PREMIUM"] = "CODICE_SERVIZIO_PREMIUM";
}
/.*TREATMENT.?CODE.*/ {
   sub(/\r$/,"");    
   insert = "INSERT INTO CORPORATEAPP.DIRECT_MARKETING(";
   for (i = 1; i <= NF; i++) {
        insert = insert columnNames[$i] ", ";
   }
   insert = substr(insert, 1, length(insert) - 2) ") VALUES (";
   columnsNum = NF;
}
! /.*TREATMENT_CODE.*/ {
   sub(/\r$/,"");
   rowInsert = insert;
   for (i = 1; i <= columnsNum; i++) {
        if (i <= NF && length($i) > 0) {
            rowInsert = rowInsert "'" $i "', ";
        } else {
            rowInsert = rowInsert "null, ";
        }
   }
   rowInsert = substr(rowInsert, 1, length(rowInsert) - 2) ");";   
   print rowInsert;
}
END{
    
}
