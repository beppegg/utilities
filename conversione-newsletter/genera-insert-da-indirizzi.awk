#!/usr/bin/awk -f
BEGIN{
    insertChunkSize = 3;
    outfileBasename = "import-newsletter";
    fileIndex = 1;
}
(NR - 1) % insertChunkSize == 0 {
    outFile = outfileBasename "." fileIndex++ ".sql";
    
    print "Generating import SQL Script for emails " NR " to " NR + insertChunkSize - 1 " into file " outFile;
    
    print "-- INSERT INTO CORPORATELOGIN.WM_USERS(USERNAME, TIPO_WM, EMAIL, INSERTDATE)" > outFile
    print "SELECT" >> outFile;
    print "    USERNAME," >> outFile;
    print "    '7days'," >> outFile;
    print "    EMAIL," >> outFile;
    print "    sysdate" >> outFile;
    print "FROM" >> outFile;
    print "    SSO.USER_DATA" >> outFile;
    print "WHERE" >> outFile;             
    print "    STATUS = 'A'" >> outFile;
    print "AND" >> outFile;
    print "    TYPE = 'C'" >> outFile;
    print "AND" >> outFile;
    printf ("%s", "    LOWER(EMAIL) IN (") >> outFile;
    tabs = ""
}
/DES_INDIRIZZO_EMAIL/ {
    next;
}
{
    sub(/\r$/,"");
    printf("%s\'%s\'", tabs, $0)  >> outFile;
    tabs = "\t\t"
}
(NR - 1)% insertChunkSize < insertChunkSize - 1 {
    print "," >> outFile
}
(NR - 1) % insertChunkSize == insertChunkSize - 1 {
    print ");" >> outFile
}
END{
    if ((NR - 1) % insertChunkSize < insertChunkSize - 1) {
        print "\'lastline-trick :-) i will not match\');" >> outFile;
    }
}
