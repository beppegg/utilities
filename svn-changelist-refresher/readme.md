# Gestore delle changelist SVN #

## Motivazione ##

Svn ha un meccanismo per "partizionare" i file in blocchi logici, su
cui effettuare poi le varie operazioni come il commit o l'update: questo
meccanismo sono le changelist.

Il meccanismo è molto comodo per suddividere, ad esempio, un insieme di
file lavorati in base alla issue d'appartenenza e raggrupparne i commit.

Le changelist sono tuttavia piuttosto limitate: i problemi principali sono
1. Il fatto che un file può appartenere soltanto ad una changelist
   alla volta;
2. Il fatto che, non appena un file viene committato, questo viene
   rimosso dalla changelist di appartenenza; ovvero le changelist
   non sopravvivono nel tempo.
   
## La soluzione ##

La soluzione è un semplice script di shell che legge file di configurazione 
da una cartella predefinita. Ciascuna configurazione  è **denominato con
il nome della changelist** e **contiene l'elenco dei file che ne fanno
parte.**

Lo script, chiamato clrefresh, prende come parametro il nome della 
changelist ed aggiunge tutti i file listati nella configurazione corrispondente
alla changelist SVN, ricreandola quindi ogni volta uguale.

## Utilità: bash completion ##

E' presente un file "*.bash_completion*" che può essere fuso con quello
presente nella cartella *home* di ciascun utente. Questo file attiva
la completion delle changelist configurate per lo script "clrefresh".

