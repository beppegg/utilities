<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	version="2.0"
	xmlns:i18n="http://www.hp.com/best/next/clientagent/service/print/template/internationalization"
	xmlns:i18n-internal="http://www.hp.com/best/next/clientagent/service/print/template/internationalization-private"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xsl xs fn i18n i18n-internal">
	
	<!--
		This XSLT module implements functions for handling multiple languages in XSLT transformations.
		Expose three main functions:
	
		* i18n:translate - Accepts a "key" for a string, returns a localized version of the strings identified by the given key
		* i18n:format - Accepts a "key" for a string, and a list of parameters that will be substitued positionally into paceholders
                             set in the string value. A placeholder is text in the shape of %identifier%. The placeholder's identifier is there
                             just for improving string's legibility, since replacement always occurs positionally (the first placeholder will be
							 substituted by the first parameter, the second placeholder will be substituted with the second parameter and
                             so on)
        * i18n:number2string - Converts a supplied integer number into its textual representation for the language.

		This module use a particular XML file as "resource bundle", named "language-def.<LANGUAGE_CODE>.xml"
	-->

	<xsl:variable name="language" as="node()" select="document(fn:concat('language-def.', $lang, '.xml'))" />
	<xsl:key name="i18nTranslations" match="translation" use="@for" />
	<xsl:key name="i18nNumbers" match="number" use="@is" />
	
	
	<xsl:function name="i18n:translate" as="xs:string">
		<xsl:param name="key" as="xs:string" />
		
		<xsl:for-each select="$language">
			<xsl:value-of select="fn:key('i18nTranslations', $key)/@is" />
		</xsl:for-each>
	</xsl:function>
	
	<xsl:function name="i18n:format" as="xs:string">
		<xsl:param name="key" as="xs:string" />
		<xsl:param name="params" as="xs:string*" />
		
		<xsl:variable name="splitI18Nstring" as="xs:string*" select="fn:tokenize(i18n:translate($key), '%\w+%')" />

		<xsl:variable name="buildingBlocks" as="xs:string*">
			<xsl:for-each select="$splitI18Nstring">
				<xsl:variable name="currentStep" as="xs:integer" select="fn:position()" />
				<xsl:value-of select="fn:current()" />
				<xsl:value-of select="$params[$currentStep]" />
			</xsl:for-each>
		</xsl:variable>
		
		<xsl:value-of select="fn:string-join($buildingBlocks, '')" />
	</xsl:function>

  	<xsl:function name="i18n:number2string" as="xs:string">
		<xsl:param name="number" as="xs:integer" />
		<xsl:value-of select="i18n-internal:number2string($number, 0)" />
	</xsl:function>
	
	<xsl:function name="i18n-internal:number2string" as="xs:string">
		<xsl:param name="number" as="xs:integer" />
		<xsl:param name="order" as="xs:integer" />
		
		<xsl:variable name="moreThanHundreds" as="xs:integer" select="$number idiv 1000" />
		<xsl:variable name="hundreds" as="xs:integer" select="$number mod 1000" />
		
		<xsl:variable name="singularOrderNames" as="xs:string*">
			<xsl:choose>
				<xsl:when test="$moreThanHundreds = 1">
					<xsl:sequence select=" 'thousand' " />
					<xsl:sequence select=" 'million' " />
					<xsl:sequence select=" 'billion' " />
				</xsl:when>
				<xsl:otherwise>
					<xsl:sequence select=" 'thousands' " />
					<xsl:sequence select=" 'millions' " />
					<xsl:sequence select=" 'billions' " />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="translationParts" as="xs:string*" >
			<xsl:if test="$moreThanHundreds gt 0">
				<xsl:value-of select="i18n-internal:number2string($moreThanHundreds, $order + 1)" />
			</xsl:if>
			<xsl:value-of select="if ($moreThanHundreds = 0 or $hundreds gt 0) then i18n-internal:translateHundreds($hundreds, $order) else ''" />
		</xsl:variable>
		
		<xsl:value-of select="fn:string-join($translationParts, '')" />
	</xsl:function>
	
	<xsl:function name="i18n-internal:translateHundreds" as="xs:string">
		<xsl:param name="number" as="xs:integer" />
		<xsl:param name="order" as="xs:integer" />
		
		<xsl:variable name="particularTranslation" as="xs:string">
			<xsl:choose>
				<xsl:when test="$number ne 1 or $order = 0">
					<xsl:for-each select="$language">
						<xsl:value-of select="fn:key('i18nNumbers', xs:string($number))/@unit" />
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select=" '' " />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="hundredTranslationParts" as="xs:string*">
			<!-- convert number value -->
			<xsl:choose>
				<xsl:when test="$particularTranslation ne '' ">
					<xsl:value-of select="$particularTranslation" />
				</xsl:when>
				<xsl:when test="$number ne 1 or $order = 0">
					<xsl:variable name="hundreds" as="xs:integer" select="if ($number > 99) then $number idiv 100 else 0" />
					<xsl:variable name="hundredsTranslation" as="xs:string">
						<xsl:for-each select="$language">
							<xsl:value-of select="fn:key('i18nNumbers', xs:string($hundreds))/@hundreds" />
						</xsl:for-each>
					</xsl:variable>
					
					<!-- check if the tens part has a particular translation -->
					<xsl:variable name="particularTensTranslatrion" as="xs:string">
						<xsl:for-each select="$language">
							<xsl:value-of 	select="if ($number mod 100 gt 0) then fn:key('i18nNumbers', xs:string($number mod 100))/@unit else '' " />
						</xsl:for-each>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="$particularTensTranslatrion ne ''">
							<xsl:value-of select="fn:concat(
									i18n-internal:applyElision($hundredsTranslation, $particularTensTranslatrion),
									$language/language-def/numbers/@hundredsToTens,
									$particularTensTranslatrion)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="tens" as="xs:integer" select="if ($number > 99) then ($number mod 100) idiv 10 else $number idiv 10 "/>
							<xsl:variable name="units" as="xs:integer" select="$number mod 10" />
		
							<xsl:variable name="tensTranslation" as="xs:string">
								<xsl:for-each select="$language">
									<xsl:value-of select="fn:key('i18nNumbers', xs:string($tens))/@tens" />
								</xsl:for-each>
							</xsl:variable>
							<xsl:variable name="unitTranslation" as="xs:string">
								<xsl:for-each select="$language">
									<xsl:value-of select="if ($units gt 0) then fn:key('i18nNumbers', xs:string($number mod 10))/@unit else ''" />
								</xsl:for-each>
							</xsl:variable>
							<xsl:value-of select="fn:concat(
								i18n-internal:applyElision($hundredsTranslation, $tensTranslation),
								$language/language-def/numbers/@hundredsToTens,
								i18n-internal:applyElision($tensTranslation, $unitTranslation),
								$unitTranslation)" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
			</xsl:choose>
			<!-- Probably add order indicator -->
			<xsl:if test="$order gt 0">
				<xsl:variable name="singularOrPluralOrderNames" as="xs:string*">
					<xsl:choose>
						<xsl:when test="$number = 1">
							<xsl:sequence select=" 'thousand' " />
							<xsl:sequence select=" 'million' " />
							<xsl:sequence select=" 'billion' " />
						</xsl:when>
						<xsl:otherwise>
							<xsl:sequence select=" 'thousands' " />
							<xsl:sequence select=" 'millions' " />
							<xsl:sequence select=" 'billions' " />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:value-of select="$language/language-def/numbers/attribute()[local-name() = $singularOrPluralOrderNames[$order]]" />
			</xsl:if>
		</xsl:variable>
		
		<xsl:value-of select="fn:string-join($hundredTranslationParts, '')" />
	</xsl:function>
	
	<xsl:function name="i18n-internal:applyElision" as="xs:string">
		<xsl:param name="stringToElide" as="xs:string" />
		<xsl:param name="stringThatFollows" as="xs:string" />
		
		<xsl:value-of select="if (xs:boolean($language/language-def/numbers/@vowel-elision)
										  and fn:matches($stringThatFollows, '^[aeiou].*' ) ) 
						then fn:substring($stringToElide, 1, fn:string-length($stringToElide) - 1) 
						else $stringToElide" />
	</xsl:function>
</xsl:stylesheet>
