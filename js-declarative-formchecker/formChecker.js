/*=================================================*
 *=================================================*
 *===                                           ===*
 *=== Adeguamento di Internet Explorer allo     ===*
 *=== Standard Javascript                       ===*
 *===                                           ===*
 *=================================================*
 *=================================================*/

if (!Array.prototype.forEach) {
	Array.prototype.forEach = function(fun /* , thisp */) {
		var len = this.length >>> 0;
		if (typeof fun != "function")
			throw new TypeError();

		var thisp = arguments[1];
		for ( var i = 0; i < len; i++) {
			if (i in this)
				fun.call(thisp, this[i], i, this);
		}
	};
}

if (!Array.prototype.every) {
	Array.prototype.every = function(fun /* , thisp */) {
		var len = this.length >>> 0;
		if (typeof fun != "function")
			throw new TypeError();

		var thisp = arguments[1];
		for ( var i = 0; i < len; i++) {
			if (i in this && !fun.call(thisp, this[i], i, this))
				return false;
		}

		return true;
	};
}

// /////////////////////////////////////////////////////////////////////////////

/**
 * Classe "FieldController", incapsula i controlli da eseguire su un singolo
 * campo.
 * 
 * @param strFieldId
 *            ID del campo da controllare.
 * @param rxpValido
 *            espressione regolare che identifica i valori corretti per il
 *            campo. Pu� essere <code>null</code>, in tal caso il campo verr�
 *            sempre considerato valido a meno di controlli aggiuntivi.
 * @param fnAltreVerifiche
 *            funzione utilizzata per effettuare verifiche aggiuntive sul campo,
 *            che non possono essere espresse tramite un'espressionre regolare.
 *            Se specificata, la funzione deve restituire un singolo valore
 *            booleano che identifica l'accettazione o la refutazione del campo
 *            da validare. Se <code>null</code>, il campo viene sempre
 *            considerato valido a meno di verifica tramite espressione
 *            regolare.
 * @param strMessaggioErrore
 *            messaggio d'errore da visualizzare nel caso il campo non soddisfi
 *            il requisito di validit�. In caso sia <code>null</code>, verr�
 *            visualizzato un messaggio d'errore di default.
 */
function FieldController(strFieldId, rxpValido, fnAltreVerifiche,
		strMessaggioErrore) {
	this._strIdCampo = strFieldId;
	this._rxpValido = rxpValido;
	this._fnAltreVerifiche = fnAltreVerifiche;
	this._strMessaggioErrore = (strMessaggioErrore != null && strMessaggioErrore.length > 0) ? strMessaggioErrore
			: "Il campo " + strFieldId + " non � valido.";
}

FieldController.prototype._strIdCampo;
FieldController.prototype._rxpValido;
FieldController.prototype._fnAltreVerifiche;
FieldController.prototype._strMessaggioErrore;

/**
 * Verifica la correttezza del campo, applicando l'espressione regolare e
 * l'eventuale funzione aggiuntiva.
 * 
 * @return una stringa vuota nel caso il campo soddisfi il vincolo di validit�,
 *         il messaggio d'errore configuato se il campo non risulta valido.
 */
FieldController.prototype.controllaCampo = function() {
	var strMessaggioRestituito;

	// eseguo i controlli impostati
	if ((this._rxpValido == null || this._rxpValido.test(document
			.getElementById(this._strIdCampo).value))
			&& (this._fnAltreVerifiche == null || this._fnAltreVerifiche() == true)) {
		// il campo � valido, nessun messaggio d'errore
		strMessaggioRestituito = "";
	} else {
		// il campo non � valido, restituisco il messaggio d'errore.
		strMessaggioRestituito = this._strMessaggioErrore;
	}

	return strMessaggioRestituito;
};

// /////////////////////////////////////////////////////////////////////////////

/**
 * Classe "FormChecker", responsabile di effettuare i controlli su un intero
 * form. Viene configurato tramite l'invocazione dei vari metodi.
 * 
 * @param idFormGestito
 *            identificativo del form per il quale questo form checker gestisce
 *            la verifica dei dati. Se nel form non � specificato un gestore per
 *            l'evento <code>onsubmit</code>, il form checker vi si associa
 *            automaticamente.
 */
function FormChecker(idFormGestito) {
	this._controlliDaEffettuare = new Array();

	if (idFormGestito != null) {
		// esegui il controllo di validit� sul submit del form gestito
		var formChecker = this;
		var fnAssociaFormCheckerAForm = function() {
			var formGestito = document.getElementById(idFormGestito);
			if (formGestito.onsubmit == null) {
				formGestito.onsubmit = function() {
					return formChecker.controllaForm();
				};
			}
		};

		if (window.addEventListener) {
			window.addEventListener('load', fnAssociaFormCheckerAForm, false);
		} else if (document.addEventListener) {
			document.addEventListener('load', fnAssociaFormCheckerAForm, false);
		} else if (window.attachEvent) {
			window.attachEvent('onload', fnAssociaFormCheckerAForm);
		}
	}
}

FormChecker.prototype._controlliDaEffettuare;

/**
 * Aggiunge la verifica del formato di un campo "data". Sono accettate soltanto
 * le date nel formato gg/mm/aaaa. NON VIENE EFFETTUATO IL CONTROLLO SEMANTICO,
 * ovvero viene accettata la data "43/23/5024". Vengono accettate le date vuote.
 * 
 * @param fieldId
 *            l'ID del campo data da sottoporre a verifica.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo non sia una
 *            data valida. Pu� essere <code>null</code>.
 */
FormChecker.prototype.formatoData = function(fieldId, messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId,
			/^(\d{2}\/\d{2}\/\d{4})?$/, null, messaggioErrore));
	return this;
};

/**
 * Aggiunge una verifica per un campo "email".
 * 
 * @param fieldId
 *            l'ID del campo da sottoporre a verifica.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo non sia un
 *            indirizzo di posta elettronica valido. Pu� essere
 *            <code>null</code>.
 */
FormChecker.prototype.formatoEmail = function(fieldId, messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId,
			/^([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})?$/i, null,
			messaggioErrore));
	return this;
};

/**
 * Aggiunge la verifica di un campo "codice fiscale". Sono ammessi valori
 * espressi nel formato standard, oppure uguali ad una partita iva, se questa �
 * specificata.
 * 
 * @param fieldId
 *            l'ID del campo da sottoporre a verifica.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo non sia un
 *            codice fiscale valido. Pu� essere <code>null</code>.
 * @param partitaIvaFieldId
 *            ID del campo che contiene la partita IVA con la quale confrontare
 *            il codice fiscale per la validazione. Pu� essere <code>null</code>
 *            se non � prevista l'uguaglianza con una partita IVA.
 */
FormChecker.prototype.formatoCodiceFiscale = function(fieldId, messaggioErrore,
		partitaIvaFieldId) {
	this._controlliDaEffettuare
			.push(new FieldController(
					fieldId,
					null,
					function() {
						var strValoreCodiceFiscale = document
								.getElementById(this._strIdCampo).value;
						/*
						 * Controllo se rispetta il pattern standard.
						 */
						var blnCampoValido = /^([A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z])?$/
								.test(strValoreCodiceFiscale);
						if (!blnCampoValido) {
							/*
							 * Altrimenti, controllo se uguale ad una partita
							 * iva
							 */
							if (partitaIvaFieldId != null) {
								var strValorePartitaIva = document
										.getElementById(partitaIvaFieldId).value;
								blnCampoValido = (strValoreCodiceFiscale == strValorePartitaIva);
							}
						}

						return blnCampoValido;
					}, messaggioErrore));
	return this;
};

/**
 * Aggiunge la verifica di un campo "partita iva". Sono ammessi valori composti
 * da 11 caratteri numerici.
 * 
 * @param fieldId
 *            l'ID del campo da sottoporre a verifica.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo non sia una
 *            partita IVA valida. Pu� essere <code>null</code>.
 */
FormChecker.prototype.formatoPartitaIva = function(fieldId, messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId,
			/^(\d{11})?$/, null, messaggioErrore));
	return this;
};

/**
 * Verifica che un campo non sia vuoto.
 * 
 * @param fieldId
 *            l'ID del campo da sottoporre a verifica.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo non sia
 *            stato valorizzato. Pu� essere <code>null</code>.
 */
FormChecker.prototype.campoObbligatorio = function(fieldId, messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId, /.+/, null,
			messaggioErrore));
	return this;
};

/**
 * Verifica che almeno un campo tra quelli specificati non sia vuoto.
 * 
 * @param fieldIdArray
 *            Array di ID dei campi da sottoporre a verifica.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso nessun campo sia
 *            valorizzato. Pu� essere <code>null</code>.
 */
FormChecker.prototype.almenoUnoObbligatorio = function(fieldIdArray,
		messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId, null,
			function() {
				var strAccumulatoreContenutoCampi = "";
				fieldIdArray.forEach(function(currentFieldId) {
					strAccumulatoreContenutoCampi += document
							.getElementById(currentFieldId).value;
				});

				return /.+/.test(strAccumulatoreContenutoCampi);
			}, messaggioErrore));
	return this;
};

/**
 * Verifica che un campo sia vuoto o composto da soli caratteri numerici.
 * 
 * @param fieldId
 *            l'ID del campo da sottoporre a verifica.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo non sia
 *            composto da soli caratteri numerici. Pu� essere <code>null</code>.
 */
FormChecker.prototype.campoNumerico = function(fieldId, messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId, /^\d*$/,
			null, messaggioErrore));
	return this;
};

/**
 * Verifica che il campo in esame sia valorizzato se lo sono anche tutti i campi
 * specificati come "determinanti".
 * 
 * @param fieldId
 *            ID del campo da sottoporre a verifica.
 * @param idCampiDeterminanti
 *            <code>Array</code> di identificatori di campi che, se
 *            valorizzati, implicano l'obbligatoriet� del campo sotto esame.
 * @param messaggioErrore
 *            messaggio d'errore da visualizzare in caso la data sia successiva
 *            a quella limite. Pu� essere <code>null</code>.
 */
FormChecker.prototype.campoObbligatorioSePresenti = function(fieldId,
		idCampiDeterminanti, messaggioErrore) {
	this._controlliDaEffettuare
			.push(new FieldController(
					fieldId,
					null,
					function() {
						var blnAccettato = true;
						var valoreCampo = document
								.getElementById(this._strIdCampo).value;
						if (valoreCampo.length == 0) {
							/*
							 * valido solo se tutti gli altri campi sono nulli
							 */
							blnAccettato = idCampiDeterminanti
									.every(function(element, index, array) {
										return (document
												.getElementById(element).value.length == 0);
									});
						}
						return blnAccettato;
					}, messaggioErrore));
	return this;
};

/**
 * Verifica che un campo sia valorizzato con una stringa lunga non meno del
 * numero di caratteri specificato.
 * 
 * @param fieldId
 *            l'ID del campo da sottoporre a verifica.
 * @param lunghezzaMin
 *            la lunghezza minima che il valore del campo deve avere.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo sia pi�
 *            corto del valore specificato. Pu� essere <code>null</code>.
 */
FormChecker.prototype.lunghezzaMinima = function(fieldId, lunghezzaMin,
		messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId, new RegExp(
			".{" + lunghezzaMin + ",}"), null, messaggioErrore));
	return this;
};

/**
 * Verifica che un campo sia valorizzato con una stringa lunga non pi� del
 * numero di caratteri specificato.
 * 
 * @param fieldId
 *            l'ID del campo da sottoporre a verifica.
 * @param lunghezzaMax
 *            la lunghezza massima che il valore del campo deve avere.
 * @param messaggioErrore
 *            il messaggio d'errore da visualizzare in caso il campo pi� lungo
 *            del valore specificato. Pu� essere <code>null</code>.
 */
FormChecker.prototype.lunghezzaMassima = function(fieldId, lunghezzaMax,
		messaggioErrore) {
	this._controlliDaEffettuare.push(new FieldController(fieldId, new RegExp(
			".{0," + lunghezzaMax + "}"), null, messaggioErrore));
	return this;
};

/**
 * Verifica che la data esplicitata nel primo campo sia precedente e non
 * coincidente alla data contenuta nel secondo campo. Si presuppone che le date
 * sia formattate correttamente.
 * 
 * @param firstDateFieldId
 *            ID del campo data che deve risultare precedente.
 * @param secondDateFieldId
 *            ID del campo data che deve risultare successivo.
 * @param messaggioErrore
 *            messaggio d'errore da visualizzare nel caso l'ordinamento non sia
 *            rispettato. Pu� essere <code>null</code>.
 */
FormChecker.prototype.ordinamentoDate = function(firstDateFieldId,
		secondDateFieldId, messaggioErrore) {
	this._controlliDaEffettuare
			.push(new FieldController(
					firstDateFieldId,
					null,
					function() {
						var blnEsitoVerifica = true;
						var rxpData = /^(\d{2})\/(\d{2})\/(\d{4})$/;

						var strDataPrimoCampo = document
								.getElementById(this._strIdCampo).value;
						var strDataSecondoCampo = document
								.getElementById(secondDateFieldId).value;

						if (strDataPrimoCampo.length
								* strDataSecondoCampo.length > 0) {
							var arrScomposizionePrimaData = rxpData
									.exec(strDataPrimoCampo);
							var arrScomposizioneSecondaData = rxpData
									.exec(strDataSecondoCampo);

							var dtDataPrimoCampo = new Date(
									arrScomposizionePrimaData[3],
									parseInt(arrScomposizionePrimaData[2]) - 1,
									arrScomposizionePrimaData[1]);
							var dtDataSecondoCampo = new Date(
									arrScomposizioneSecondaData[3],
									parseInt(arrScomposizioneSecondaData[2]) - 1,
									arrScomposizioneSecondaData[1]);

							blnEsitoVerifica = dtDataSecondoCampo
									- dtDataPrimoCampo > 0
						}

						return blnEsitoVerifica;
					}, messaggioErrore));
	return this;
};

/**
 * Verifica che la data espressa in un campo non sia successiva alla data
 * specificata.
 * 
 * @param fieldId
 *            ID del campo da sottoporre a verifica.
 * @param dtDataMassima
 *            data massima per la quale considerare valido un campo.
 * @param messaggioErrore
 *            messaggio d'errore da visualizzare in caso la data sia successiva
 *            a quella limite. Pu� essere <code>null</code>.
 */
FormChecker.prototype.dataNonSuccessiva = function(fieldId, dtDataMassima,
		messaggioErrore) {
	this._controlliDaEffettuare
			.push(new FieldController(
					fieldId,
					null,
					function() {
						var blnEsitoControllo = true;
						var rxpData = /^(\d{2})\/(\d{2})\/(\d{4})$/;

						var strDataCampo = document
								.getElementById(this._strIdCampo).value;
						if (strDataCampo.length > 0) {
							var arrScomposizioneDataCampo = rxpData
									.exec(strDataCampo);
							var dtDataCampo = new Date(
									arrScomposizioneDataCampo[3],
									parseInt(arrScomposizioneDataCampo[2]) - 1,
									arrScomposizioneDataCampo[1]);

							blnEsitoControllo = dtDataMassima - dtDataCampo >= 0;
						}

						return blnEsitoControllo;
					}, messaggioErrore));
	return this;
};

/**
 * Effettua la verifica di una form utilizzando i controlli configurati in
 * precedenza. In caso la form sia valida, la funzione ritorna <code>true</code>.
 * In caso il form contenga campi non validi, viene visualizzato un messaggio
 * all'utente con l'elenco di tutti i messaggi d'errori generati durante il
 * controllo e viene restituito il valore <code>false</code>.
 * 
 * @return <code>true</code> se tutti i controlli danno esito positivo,
 *         <code>false</code> altrimenti.
 */
FormChecker.prototype.controllaForm = function() {
	var strMessaggiErrore = "";
	this._controlliDaEffettuare.forEach(function(currentFieldController,
			currentControllerIdx, controllersArray) {
		var currentErrorMessage = currentFieldController.controllaCampo();
		if (currentErrorMessage.length > 0 && strMessaggiErrore.length > 0) {
			strMessaggiErrore += "\n";
		}
		strMessaggiErrore += currentErrorMessage;
	});

	if (strMessaggiErrore.length > 0) {
		window.alert(strMessaggiErrore);
	}

	return strMessaggiErrore.length == 0;
};